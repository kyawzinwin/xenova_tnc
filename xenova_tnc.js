$(function() {
	var xe_agree_count = 0;

	$.fn.xenova = function() {
		var xe_data = this.hide(),
			xe_overlay = '<div class="xe_overlay"><span class="xe_overlay_close">X</span></div>',
			xe_theme = {
				width: "400px",
				border: "10px solid #bbb",
				padding: "40px",
				margin: "20px auto",
				background: "#ddd",
				display: "block"
			},
		init = function() {
			$('body')
				.append( xe_overlay )
				.find('div.xe_overlay')
				// .append( xe_data.show().addClass("xe_wrap") )
				.append( xe_data.show().css( xe_theme ).append('<div class="xe_foot" id="xe_agree">Agree</div>') )
				.fadeIn(100);
		}
		
		init();
		console.log( xe_data );
		// var xe_data = this.addClass("xe_wrap");
		// $('body')
		// 	.append('<div class="xe_overlay"></div>')
		// 	.find('div.xe_overlay')
		// 	.append(xe_data)
		// 	.fadeIn(500);
	}

	$('#trigger').on('click', function() {
		$( "#terms" ).xenova();	
	});

	// fade out .overlay and remove element in callback
	/* version 1 fade out and remove by clicking black area */

	// $(document).on("click", ".xe_overlay", function(e) {
	// 	$(this).fadeOut(100, function() {
	// 		$('#xe_mainwrapper').append( $('#terms') );
	// 		$(this).remove();
	// 	});
	// });

	/* version 2 fade out and remove by clicking x button */
	$(document).on("click", ".xe_overlay_close", function(e) {
		$(this).parent().fadeOut(100, function() {
			$('#xe_mainwrapper').append( $('#terms') );
			$(this).remove();
		});
	});

	$(document).on("click", "#xe_agree", function(e) {
		if( xe_agree_count == 0 ) {
			alert('Please make sure to read every words in T&C');
			xe_agree_count ++;	
		} else if( xe_agree_count == 1) {
			$(".xe_overlay_close").parent().fadeOut(100, function() {
				$('#xe_mainwrapper').append( $('#terms') );
				$(this).remove();
			});	
		}
		
		// $(this).parent().fadeOut(100, function() {
		// 	$('#xe_mainwrapper').append( $('#terms') );
		// 	$(this).remove();
		// });
	});
	

	// var data = $('#terms');

	// // append .overlay and fade it in
	// $('button#trigger').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('body')
	// 		.append('<div class="overlay"></div>')
	// 		.find('div.overlay')
	// 		.append(data)
	// 		.fadeIn(500);
	// });

	// // fade out .overlay and remove element in callback
	// $(document).on("click", ".overlay", function(e) {
	// 	$(this).fadeOut(500, function() {
	// 		$(this).remove();
	// 	});
	// });

})